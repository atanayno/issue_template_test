---
module-name: "New Support Team Member Start Here"
area: "Customer Service"
---

## Introduction

Welcome to the Support Team, we are so excited that you've joined us!

> **As you work through this onboarding issue, remember that this issue is [non-confidential and is public](https://about.gitlab.com/handbook/values/#transparency). Please do not add any confidential data such as customer names, logs, etc. to this issue. Linking to the appropriate [Zendesk ticket](https://about.gitlab.com/handbook/support/workflows/working_with_security.html#general-guidelines-1) is OK!**

**Your [Onboarding Buddy](https://about.gitlab.com/handbook/support/training/onboarding_buddy.html):** `[tag buddy]`

**Goals of this checklist**

Keep this issue open until you complete the onboarding pathway modules shown below for your role to track your onboarding modules. Once you have completed them and had a discussion with your manager, close this issue.

**General Timeline and Expectations** 

- Read about our [Support Onboarding process](https://about.gitlab.com/handbook/support/training/); the page also shows you the different modules you'll need to complete as part of your onboarding, duration, and other general information.
- Your [Onboarding Buddy](https://about.gitlab.com/handbook/support/training/onboarding_buddy.html) is your primary contact-person during your onboarding. They will schedule regular check-ins with you, and are able to guide you through your [Onboarding Modules](https://about.gitlab.com/handbook/support/training/#support-onboarding-pathway).
- It should take you approximately **5 to 6 weeks to complete** all the modules that make up your onboarding pathway.

1. [ ] Edit this issue's description to remove the irrelevant path based on your role below.

## Support Engineer onboarding pathway

Note: When you've completed the onboarding pathway, speak with your manager and choose your first area of focus (usually "GitLab.com SAAS Support" or "Self-Managed Support")

- Each of the modules listed below have an issue template. To create your training issue, create a new issue in the project, in the Description dropdown select the related template, set the title to  "`Your Name`: `module name`", assign it to yourself, and save.

1. New Support Team Member Start Here
1. [ ] Git & GitLab Basics
1. [ ] Installation & Administration Basics
1. [ ] GitLab Support Basics
1. [ ] Customer Service Skills
1. [ ] Zendesk Basics
1. [ ] Working on Tickets
1. [ ] Documentation  - Completion not required before closing the onboarding issue  
